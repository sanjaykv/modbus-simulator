"""
Copyright (c) 2017 Riptide IO, Inc. All Rights Reserved.

"""
from __future__ import absolute_import, unicode_literals

__VERSION__ = "2.0.0"
